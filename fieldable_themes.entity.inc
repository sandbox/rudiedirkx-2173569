<?php

class ThemeEntityController extends EntityAPIController {
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    return parent::save($entity, $transaction);
  }

  public function load($ids = array(), $conditions = array()) {
    // These are keyed by entity id.
    $raw_entities = parent::load($ids, $conditions);
    return $raw_entities;

    $idKey = $this->areMachineNames($ids) ? 'machine_name' : 'tid';

    // Add theme info and key by machine name.
    $themes = list_themes();
    $keyed_entities = array();
    foreach ($raw_entities as $entity) {
      $entity->theme = $themes[$entity->machine_name];
      $keyed_entities[$entity->$idKey] = $entity;
    }
    $this->cacheSet($keyed_entities);

    // Sort the same as input. Normally parent::load() would do this, but not now, because
    // of the idKey trick, so we have to do it again.
    $ordered_entities = array();
    foreach ($ids as $id) {
      if (isset($keyed_entities[$id])) {
        $ordered_entities[$id] = $keyed_entities[$id];
      }
    }

    return $ordered_entities;
  }

  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    // Trick entity temporarily into thinking 'machine_name' is the primary key, for
    // a much faster query that doesn't require id <> machine name conversion.
    $idKey = $this->idKey;
    if ($this->areMachineNames($ids)) {
      // But only if machine names were passed in.
      $this->idKey = 'machine_name';
    }
    $query = DrupalDefaultEntityController::buildQuery($ids, $conditions, $revision_id);
    $this->idKey = $idKey;
    return $query;
  }

  protected function areMachineNames($ids) {
    return !is_numeric(reset($ids));
  }
}

class ThemeEntity extends Entity {}
