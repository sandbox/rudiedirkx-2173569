<?php

class views_plugin_argument_default_current_theme extends views_plugin_argument_default {

  function get_argument() {
    global $theme;
    $themes = list_themes();
    $use_theme = !empty($themes[$theme]->status) ? $theme : variable_get('theme_default');

    // Return string or id, depending on the matching field.
    $match_field = $this->argument->real_field;
    return $match_field == 'machine_name' ? $use_theme : _theme_get_id($use_theme);
  }

}
